from fastapi import FastAPI
from celery_worker import get_factorial
from fastapi.responses import HTMLResponse

app = FastAPI()


def int_or_false(something):
    """
    Tries to return an integer object constructed from a number or string,
    and if this is not possible, returns False
    """

    try:
        result = int(something)
        return result
    except (ValueError, TypeError):
        return False


@app.get('/')
def index(num=None):
    """
    If num is an integer, it returns its factorial, otherwise returns html with the number input form
    """
    num = int_or_false(num)
    if num:
        result = get_factorial.delay(num)
        return result.get()
    else:
        content = '''
        <!DOCTYPE HTML>
            <html>
             <head>
              <meta charset="utf-8">
              <title>Запрос факториала</title>
             </head>
             <body>
              <form action="/">
               <p>Введите целое число для вычисления факториала</p>
               <p><input name="num"> </p>
               <p><input type="submit"></p>
              </form>
             </body>
            </html>
        '''
    return HTMLResponse(content, status_code=200)
