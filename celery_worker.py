from functools import reduce
from celery import Celery

celery = Celery('celery_worker',
                broker='amqp://localhost:5672',
                backend='rpc://',
                )


@celery.task
def get_factorial(x: int) -> int:
    iter = frozenset(i for i in range(1, x + 1))
    def calculate(i, j): return i * j
    return reduce(calculate, iter)



